import pinkBackground from '../assets/images/pink-bg.jpg'
import orangeBackground from '../assets/images/orange-bg.jpg'

const Image = {
  PINK_BG: pinkBackground,
  ORANGE_BG: orangeBackground
}

export default Image
import React from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import {
  AppBar,
  Container,
  Box,
  Toolbar,
  Typography,
  Button,
  Tooltip,
  IconButton,
  Avatar,
  Menu,
  MenuItem,
  ListItemIcon,
} from "@mui/material";
import { headerStyle } from "./style";
import { useDispatch, useSelector } from "react-redux";
import { Logout } from "@mui/icons-material";
import { logout } from "../../reducers/authSlice";

function Header() {
  const classes = headerStyle();
  const { user } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const navigate = useNavigate();
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.removeItem("user");
    dispatch(logout());
    navigate("/login");
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Container>
          <Toolbar>
            <Typography
              variant="h6"
              component={Link}
              to={"/"}
              color="inherit"
              sx={{ flexGrow: 1 }}
              className={classes.home}
            >
              Home
            </Typography>
            <Button
              color="inherit"
              component={NavLink}
              to={"/insurrance"}
              className={classes.link}
            >
              Insurrance
            </Button>
            {user ? (
              <>
                <Tooltip title="Account settings">
                  <IconButton onClick={handleClick} size="small" sx={{ ml: 2 }}>
                    <Avatar alt={user.username} />
                  </IconButton>
                </Tooltip>
                <Menu
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  onClick={handleClose}
                  transformOrigin={{ horizontal: "left", vertical: "top" }}
                  anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
                >
                  <MenuItem onClick={handleLogout}>
                    <ListItemIcon>
                      <Logout fontSize="small" />
                    </ListItemIcon>
                    Logout
                  </MenuItem>
                </Menu>
              </>
            ) : (
              <Button
                color="inherit"
                component={NavLink}
                to={"/login"}
                className={classes.link}
              >
                Login
              </Button>
            )}
          </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
}

export default Header;

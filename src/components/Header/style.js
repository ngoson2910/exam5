import { orange } from "@mui/material/colors";
import { makeStyles } from "@mui/styles";


export const headerStyle = makeStyles( theme => ({
  home: {
    textDecoration: 'none'
  },
  link: {
    '&.active': {
      color: orange[500]
    }
  }
}))
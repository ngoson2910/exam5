import React from 'react'
import PropTypes from 'prop-types'
import { Box, Typography } from '@mui/material'
import { bannerStyle } from './style'
import Image from '../../constants/image'

Banner.propTypes = {
  title: PropTypes.string,
  backgroundUrl: PropTypes.string,
}

Banner.defaultProps = {
  title: '',
  backgroundUrl: Image.ORANGE_BG
}

function Banner(props) {
  const {title} = props
  const classes = bannerStyle(props)
  return (
    <Box className={classes.banner}>
        <Typography variant='h3' color="white">
          {title}
        </Typography>
    </Box>
  )
}

export default Banner

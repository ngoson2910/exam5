import { makeStyles } from "@mui/styles";


export const bannerStyle = makeStyles(theme => ({
  banner: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
    alignItems: 'center',
    height: 250,

    backgroundImage: props => `url(${ props.backgroundUrl})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover'
  }
}))
import { makeStyles } from "@mui/styles";
import {STEP_ONE, STEP_TWO} from '../../../constants/step'


export const SideBarStyle = makeStyles(theme => ({
  bgMenuPink: {
    backgroundColor: currentStep => currentStep === STEP_ONE ? '#C4B4CA': '#fff',
    borderRadius: currentStep => currentStep === STEP_ONE ? '0 50px 50px 0': 0,
    width: currentStep => currentStep === STEP_ONE ? '110% !important': '100%',
  },
  bgMenuPink1: {
    '&:hover': {
      backgroundColor: 'unset !important'
    }
  },
  iconWrapperPink: {
    backgroundColor: currentStep => currentStep === STEP_ONE ? '#fff' : '',
    minWidth: 'unset !important',
    marginRight: '20px',
    padding: '10px',
    borderRadius: '4px'
  },
  iconPink: {
    color: '#7563A8'
  },
  textPink: {
    maxWidth: currentStep => currentStep === STEP_ONE ? '60%': 'unset',
    color: currentStep => currentStep === STEP_ONE ? '#7563A8' : '#333'
  },
  bgMenuGreen: {
    backgroundColor: currentStep => currentStep === STEP_TWO ? '#A7D8C1': '#fff',
    borderRadius: currentStep => currentStep === STEP_TWO ? '0 50px 50px 0': 0,
    width: currentStep => currentStep === STEP_TWO ? '110% !important': '100%',
  },
  bgMenuGreen1: {
    '&:hover': {
      backgroundColor: 'unset !important'
    }
  },
  iconWrapperGreen: {
    backgroundColor: currentStep => currentStep === STEP_TWO ? '#fff' : '',
    minWidth: 'unset !important',
    marginRight: '20px',
    padding: '10px',
    borderRadius: '4px'
  },
  iconGreen: {
    color: '#3A644A'
  },
  textGreen: {
    maxWidth: currentStep => currentStep === STEP_TWO ? '60%': 'unset',
    color: currentStep => currentStep === STEP_TWO ? '#3A644A' : '#333'
  },
  done: {
    color: 'green',
    marginRight: '10px'
  }

}))
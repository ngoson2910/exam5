import { Box, List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material'
import React from 'react'
import {SideBarStyle} from './style'
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import SavingsIcon from '@mui/icons-material/Savings';
import DoneIcon from '@mui/icons-material/Done';
import { useDispatch, useSelector } from 'react-redux';
import {setStep} from '../../../reducers/stepSlice'
import {STEP_ONE, STEP_TWO} from '../../../constants/step'


function SideBar() {
  const { currentStep} = useSelector(state => state.step)
  const eduState = useSelector(state => state.eduPlan)
  const protectState = useSelector(state => state.productEdu)
  const dispatch = useDispatch();
  const classes = SideBarStyle(currentStep)
  return (
    <Box>
      <List>
          <ListItem className={classes.bgMenuPink} onClick={() => dispatch(setStep(STEP_ONE))}>
            <ListItemButton className={classes.bgMenuPink1}>
              {protectState.total > 0 && <DoneIcon className={classes.done} />}
              <ListItemIcon className={classes.iconWrapperPink}>
                <VerifiedUserIcon className={classes.iconPink} />
              </ListItemIcon>
              <ListItemText primary="Bảo vệ kế hoạch giáo dục cho con" className={classes.textPink} />
            </ListItemButton>
          </ListItem>
          <ListItem className={classes.bgMenuGreen} onClick={() => dispatch(setStep(STEP_TWO))}>
            <ListItemButton className={classes.bgMenuGreen1}>
            {eduState.total > 0 && <DoneIcon className={classes.done} />}
              <ListItemIcon className={classes.iconWrapperGreen}>
                <SavingsIcon className={classes.iconGreen} />
              </ListItemIcon>
              <ListItemText primary="Tích lũy cho kế hoạch giáo dục" className={classes.textGreen} />
            </ListItemButton>
          </ListItem>
        </List>
    </Box>
  )
}

export default SideBar

import { makeStyles } from "@mui/styles";


export const AccumulationEducationStyle = makeStyles(theme => ({
  title: {
    textTransform: 'uppercase',
    marginBottom: '30px !important'
  },
  text: {
    textTransform: 'uppercase',
    textDecoration: 'underline',
    width: '50%'
  },
  paper: {
    minHeight: "500px",
    padding: "20px",
  }
}))
import { Box, Button, FormGroup, Paper, Typography } from "@mui/material";
import { FastField, Field, FieldArray, Form, Formik } from "formik";
import React, { useRef, useState } from "react";
import * as Yup from 'yup'
import InputCount from "../../../customFields/InputCount";
import { AccumulationEducationStyle } from "./style";
import InputGroupAccumulation from "../../InputGroupAccumulation";
import { useDispatch, useSelector } from "react-redux";
import { setStep } from "../../../reducers/stepSlice";
import { STEP_ONE } from "../../../constants/step";
import {initialValuesEdu, setFormValueEdu} from '../../../reducers/eduPlanSlice'
import {setFormValueProtect, initialValuesProtect} from '../../../reducers/productEduSlice'

function AccumulationEducationPlan() {
  const { children, total, childrenDetails } = useSelector((state) => state.eduPlan);
  const protectEduState = useSelector(state => state.productEdu)
  const [child, setChild] = useState(children);
  const [totalEduPlan, setTotalEduPlan] = useState(0);
  const classes = AccumulationEducationStyle();
  const dispatch = useDispatch();
  const arrayHelperRef = useRef(null);
  const valuesRef = useRef(null);
  const formRef = useRef(null);
  const initialValues = {
    children: child,
    childrenDetails: childrenDetails,
  };
  const validationSchema = Yup.object({
    childrenDetails: Yup.array(Yup.object({
      childage: Yup.number().required('This field is required!'),
      isSchool: Yup.boolean().when(['isUniversity', 'isAfterUniversity'], {
        is: (isUniversity, isAfterUniversity) => {
          return (!isUniversity && !isAfterUniversity)
        },
        then: Yup.boolean().required().isTrue('must choose one')
      }),
      schoolFee: Yup.number().when('isSchool', {
        is: true,
        then: Yup.number().required('This field is required!')
      }),
      schoolYear: Yup.number().when('isSchool', {
        is: true,
        then: Yup.number().required('This field is required!')
      }),
      isUniversity: Yup.boolean(),
      universityFee: Yup.number().when('isUniversity', {
        is: true,
        then: Yup.number().required('This field is required!')
      }),
      universityYear: Yup.number().when('isUniversity', {
        is: true,
        then: Yup.number().required('This field is required')
      }),
      isAfterUniversity: Yup.boolean(),
      afterUniversityFee: Yup.number().when('isAfterUniversity', {
        is: true,
        then: Yup.number().required('This field is required')
      }),
      afterUniversityYear: Yup.number().when('isAfterUniversity', {
        is: true,
        then: Yup.number().required('This field is required')
      }),
    })).min(1)
  })
  const handleIncrement = () => {
    const values = formRef.current.values
    let newChidrenDetailsEdu = [...values.childrenDetails]
    newChidrenDetailsEdu.push(initialValuesEdu.childrenDetails[0])
    let newChidrenDetailsProtect = [...initialValuesProtect.childrenDetails]
    newChidrenDetailsProtect.push(initialValuesProtect.childrenDetails[0])
    setChild((pre) => {
      const result = pre + 1;
      formRef.current.setFieldValue('children', result)
      dispatch(setFormValueEdu({
        children: result,
        total: totalEduPlan,
        childrenDetails: newChidrenDetailsEdu
      }))
      dispatch(setFormValueProtect({
        children: result,
        childrenDetails: newChidrenDetailsProtect
      }))
      return result;
    });
    arrayHelperRef.current.push(initialValuesEdu.childrenDetails[0]);
  };
  const handleDecrement = () => {
    setChild((pre) => {
      let result;
      if (pre === 1) {
        result = 1;
      } else {
        result = pre - 1;
      }
      arrayHelperRef.current.remove(result);
      formRef.current.setFieldValue('children', result)
      const childrenDetails = [...valuesRef.current.childrenDetails].filter(
        (_, index) => index !== pre - 1
      );
      const totalEduPlanArr = childrenDetails.map((detail, index) => {
        let total = 0;
        if (detail.isSchool)
          total += parseFloat(detail.schoolFee) * parseFloat(detail.schoolYear);
        if (detail.isUniversity)
          total +=
            parseFloat(detail.universityFee) *
            parseFloat(detail.universityYear);
        if (detail.isAfterUniversity)
          total +=
            parseFloat(detail.afterUniversityFee) *
            parseFloat(detail.afterUniversityYear);
        return total;
      });
      setTotalEduPlan(totalEduPlanArr.reduce((sum, item) => (sum += item), 0));
      dispatch(setFormValueEdu({
        children: result,
        total: totalEduPlan,
        childrenDetails: childrenDetails
      }))

      const chidrenDetailsProtect = [...protectEduState.childrenDetails]
      const newChidrenDetailsProtect = chidrenDetailsProtect.filter((_, index) => index !== pre - 1)
      dispatch(setFormValueProtect({
        children: result,
        childrenDetails: newChidrenDetailsProtect
      }))

      return result;
    });
  };

  const handleChangeFeeAndYear = (e) => {
    const childrenDetails = [...valuesRef.current.childrenDetails];
    const totalEduPlanArr = childrenDetails.map((detail, key) => {
      let total = 0;
      const name = e.target.name.split(".").pop();
      const index = +e.target.name.split(".")[1];
      const value = e.target.value;
      if (key === index) {
        switch (name) {
          case "schoolFee":
            if (detail.isSchool)
              total += parseFloat(value) * parseFloat(detail.schoolYear);
            if (detail.isUniversity)
              total +=
                parseFloat(detail.universityFee) *
                parseFloat(detail.universityYear);
            if (detail.isAfterUniversity)
              total +=
                parseFloat(detail.afterUniversityFee) *
                parseFloat(detail.afterUniversityYear);
            break;
          case "schoolYear":
            if (detail.isSchool)
              total += parseFloat(detail.schoolFee) * parseFloat(value);
            if (detail.isUniversity)
              total +=
                parseFloat(detail.universityFee) *
                parseFloat(detail.universityYear);
            if (detail.isAfterUniversity)
              total +=
                parseFloat(detail.afterUniversityFee) *
                parseFloat(detail.afterUniversityYear);
            break;
          case "universityFee":
            if (detail.isSchool)
              total +=
                parseFloat(detail.schoolFee) * parseFloat(detail.schoolYear);
            if (detail.isUniversity)
              total += parseFloat(value) * parseFloat(detail.universityYear);
            if (detail.isAfterUniversity)
              total +=
                parseFloat(detail.afterUniversityFee) *
                parseFloat(detail.afterUniversityYear);
            break;
          case "universityYear":
            if (detail.isSchool)
              total +=
                parseFloat(detail.schoolFee) * parseFloat(detail.schoolYear);
            if (detail.isUniversity)
              total += parseFloat(detail.universityFee) * parseFloat(value);
            if (detail.isAfterUniversity)
              total +=
                parseFloat(detail.afterUniversityFee) *
                parseFloat(detail.afterUniversityYear);
            break;
          case "afterUniversityFee":
            if (detail.isSchool)
              total +=
                parseFloat(detail.schoolFee) * parseFloat(detail.schoolYear);
            if (detail.isUniversity)
              total +=
                parseFloat(detail.universityFee) *
                parseFloat(detail.universityYear);
            if (detail.isAfterUniversity)
              total +=
                parseFloat(value) * parseFloat(detail.afterUniversityYear);
            break;
          case "afterUniversityYear":
            if (detail.isSchool)
              total +=
                parseFloat(detail.schoolFee) * parseFloat(detail.schoolYear);
            if (detail.isUniversity)
              total +=
                parseFloat(detail.universityFee) *
                parseFloat(detail.universityYear);
            if (detail.isAfterUniversity)
              total +=
                parseFloat(detail.afterUniversityFee) * parseFloat(value);
            break;
          default:
            break;
        }
      } else {
        if (detail.isSchool)
          total += parseFloat(detail.schoolFee) * parseFloat(detail.schoolYear);
        if (detail.isUniversity)
          total +=
            parseFloat(detail.universityFee) *
            parseFloat(detail.universityYear);
        if (detail.isAfterUniversity)
          total +=
            parseFloat(detail.afterUniversityFee) *
            parseFloat(detail.afterUniversityYear);
      }

      return total;
    });
    setTotalEduPlan(totalEduPlanArr.reduce((sum, item) => (sum += item), 0));
  };

  const handleBlurInput = () => {
    const values = formRef.current.values
    dispatch(setFormValueEdu({
      ...values,
      total: totalEduPlan
    }));
    let newChidrenDetails = [...protectEduState.childrenDetails];
    let newChildrenDetailsProtect = [...values.childrenDetails].map((detail, index) => {
      let newObj = {};
      Object.keys(detail).forEach((e, i) => {
        if(newChidrenDetails[index].hasOwnProperty(e)) {
          newObj[e] = parseInt(detail[e])
        }
      })
      return newObj;
    });
    newChidrenDetails = newChidrenDetails.map((detail, index) => {
      return {
        ...detail,
        ...newChildrenDetailsProtect[index]
      }
    })
    dispatch(setFormValueProtect({
      children: values.children,
      childrenDetails: newChidrenDetails
    }))
  }
  return (
    <Box>
      <Formik 
        initialValues={initialValues}
        validationSchema={validationSchema}
        innerRef={formRef}
        onSubmit={(values) => {
          const eduPlan = {...values};
          eduPlan.childrenDetails = eduPlan.childrenDetails.map((detail, index) => {
            Object.keys(detail).forEach((key, i) => {
              if(typeof detail[key] !== 'boolean') {
                detail[key] = parseFloat(detail[key])
              }
            })
            return detail;
          })
          dispatch(setFormValueEdu({
            ...values,
            total: totalEduPlan
          }))
          let chidrenDetailsProtect = [...protectEduState.childrenDetails];
          let newChildrenDetailsProtect = [...values.childrenDetails].map((detail, index) => {
            let newObj = {};
            Object.keys(detail).forEach((e, i) => {
              if(chidrenDetailsProtect[index].hasOwnProperty(e)) {
                newObj[e] = parseInt(detail[e])
              }
            })
            return newObj;
          });
          chidrenDetailsProtect = chidrenDetailsProtect.map((detail, index) => {
            return {
              ...detail,
              ...newChildrenDetailsProtect[index]
            }
          })
          dispatch(setFormValueProtect({
            children: values.children,
            childrenDetails: chidrenDetailsProtect
          }))
        }}
      >
        {({ values }) => {
          valuesRef.current = values;
          return (
            <Form>
              <Paper variant="elevation" className={classes.paper}>
                <Typography variant="h5" className={classes.title}>
                  Kế hoạch tích lũy cho giáo dục của con
                </Typography>
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Typography variant="body1">
                    Gia đình bạn có bao nhiêu con ?
                  </Typography>
                  <Field
                    name="children"
                    component={InputCount}
                    onIncrement={handleIncrement}
                    onDecrement={handleDecrement}
                    count={child}
                  />
                </Box>
                <FieldArray name="childrenDetails">
                  {(arrayHelper) => {
                    arrayHelperRef.current = arrayHelper;
                    return (
                      <>
                        {values.childrenDetails.map((_, index) => (
                          <InputGroupAccumulation
                            key={index}
                            label={`Con thứ ${index + 1}`}
                            index={index}
                            childrenDetails={values.childrenDetails[index]}
                            onChangeFeeAndYear={handleChangeFeeAndYear}
                            onCustomBlur={handleBlurInput}
                          />
                        ))}
                      </>
                    );
                  }}
                </FieldArray>

                <Box
                  sx={{
                    marginTop: "20px",
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Typography variant="h6" className={classes.text}>
                    Số tiền bạn cần tích lũy để đảm bảo cho kế hoạch giáo dục
                    các con:
                  </Typography>
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      backgroundColor: "#90CBB4",
                      padding: "10px 50px",
                      borderRadius: "10px",
                      marginLeft: "20px",
                    }}
                  >
                    <Typography
                      variant="h3"
                      sx={{ color: "#3E8E3E", marginRight: "10px" }}
                    >
                      {totalEduPlan}
                    </Typography>
                    <Typography variant="h6" sx={{ color: "#3E8E3E" }}>
                      Triệu đồng
                    </Typography>
                  </Box>
                </Box>
              </Paper>

              <Box
                sx={{
                  display: "flex",
                  marginTop: "30px",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Button
                  variant="outlined"
                  sx={{ marginRight: "20px" }}
                  onClick={() => dispatch(setStep(STEP_ONE))}
                >
                  Quay lại
                </Button>
                <Button
                  type="submit"
                  variant="contained"
                  color="secondary"
                  disabled={!(totalEduPlan > 0)}
                >
                  Hoàn thành
                </Button>
              </Box>
            </Form>
          );
        }}
      </Formik>
    </Box>
  );
}

export default AccumulationEducationPlan;

import { makeStyles } from "@mui/styles";


export const ProtectEducationStyle = makeStyles( theme => ({
  title: {
    textTransform: 'uppercase',
    marginBottom: '30px !important'
  },
  text: {
    textTransform: 'uppercase',
    textDecoration: 'underline'
  },
  payper: {
    minHeight: "500px",
    padding: "20px",
  }
}))
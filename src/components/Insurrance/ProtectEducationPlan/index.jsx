import { Box, Button, Paper, Typography } from "@mui/material";
import { FastField, Field, FieldArray, Form, Formik } from "formik";
import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from 'yup';
import InputButtonGroup from "../../../customFields/InputButtonGroup";
import InputCount from "../../../customFields/InputCount";
import InputGroupProtect from "../../InputGroupProtect";
import { ProtectEducationStyle } from "./style";
import { setStep } from "../../../reducers/stepSlice";
import { STEP_TWO } from "../../../constants/step";
import {setFormValueProtect, initialValuesProtect} from '../../../reducers/productEduSlice'
import {setFormValueEdu, initialValuesEdu} from '../../../reducers/eduPlanSlice'

function ProtectEducationPlan() {
  const { children, total, eduPlanAge, childrenDetails } = useSelector((state) => state.productEdu);
  const eduState = useSelector(state => state.eduPlan)
  const [child, setChild] = useState(children);
  const [totalProtect, setTotalProtect] = useState(total);
  const classes = ProtectEducationStyle();
  const dispatch = useDispatch();
  const arrayHelperRef = useRef(null);
  const valuesRef = useRef(null);
  const formRef = useRef(null);
  const initialValues = {
    children: children,
    total: total,
    eduPlanAge: eduPlanAge,
    childrenDetails: childrenDetails
  };

  const validationSchema = Yup.object({
    eduPlanAge: Yup.number().typeError('you must specify a number')
    .required('This field is required')
    .test('should-be-greather-than-yourField2', 'Should be greather than max childage', function(value) {
      const otherFieldValue = this.parent.childrenDetails
    
      if (!otherFieldValue) {
        return true;
      }
      const result = otherFieldValue.map((detail, index) => {
        let max = 0;
        Object.keys(detail).map((e,i) => {
          if(e === 'childage' && max < detail[e]) {
            max = detail[e]
          }
        })
        return max
      })
      let max = 0;
      result.map((item, i) => {
        if(max < item) {
          max = item
        }
      })
      return value > max;
    }),
    childrenDetails: Yup.array().of(
      Yup.object({
        childage: Yup.number().typeError('you must specify a number').required('This field is required').moreThan(0, 'commision should not be zero or less than zero'),
        tuition: Yup.number().typeError('you must specify a number').required('This field is required')
      })
    ).required().min(1)
  })
  const handleIncrement = () => {
    const values = formRef.current.values
    let newChidrenDetails = [...values.childrenDetails]
    newChidrenDetails.push(initialValuesProtect.childrenDetails[0])
    let newChidrenDetailsEdu = [...initialValuesEdu.childrenDetails]
    newChidrenDetailsEdu.push(initialValuesEdu.childrenDetails[0])
    setChild((pre) => {
      const result = pre + 1;
      formRef.current.setFieldValue('children', result)
      dispatch(setFormValueProtect({
        children: result,
        total: totalProtect,
        eduPlanAge: values.eduPlanAge,
        childrenDetails: newChidrenDetails
      }))
      dispatch(setFormValueEdu({
        children: result,
        childrenDetails: newChidrenDetailsEdu
      }))
      return result;
    });
    arrayHelperRef.current.push(initialValuesProtect.childrenDetails[0]);
  };
  const handleDecrement = () => {
    setChild((pre) => {
      let result;
      if (pre === 1) {
        result = 1;
      } else {
        result = pre - 1;
      }
      arrayHelperRef.current.remove(result);
      formRef.current.setFieldValue('children', result)
      const eduPlanAge = valuesRef.current.eduPlanAge;
      const childrenDetails = [...valuesRef.current.childrenDetails].filter((_, index) => index !== pre - 1);
      const totalProtectArr = childrenDetails.map((detail, index) => {
        if(
          !isNaN(parseFloat(eduPlanAge)) &&
          !isNaN(parseFloat(detail.childage)) &&
          !isNaN(parseFloat(detail.tuition))
        ) {
          return (
            (parseFloat(eduPlanAge) - parseFloat(detail.childage)) *
            parseFloat(detail.tuition) *
            12
          );
        }
        return 0
      });
      setTotalProtect(totalProtectArr.reduce((sum, item) => (sum += item), 0));
      dispatch(setFormValueProtect({
        children: result,
        total: totalProtect,
        eduPlanAge: eduPlanAge,
        childrenDetails: childrenDetails
      }))

      const chidrenDetailsEdu = [...eduState.childrenDetails]
      const newChidrenDetailsEdu = chidrenDetailsEdu.filter((_, index) => index !== pre - 1)
      dispatch(setFormValueEdu({
        children: result,
        childrenDetails: newChidrenDetailsEdu
      }))
      return result;
    });
  };

  const handleChangeEduPlanAge = (e) => {
    const eduPlanAge = e.target.value;
    const childrenDetails = [...valuesRef.current.childrenDetails];
    const totalProtectArr = childrenDetails.map((detail, index) => {
      if(
        !isNaN(parseFloat(eduPlanAge)) &&
          !isNaN(parseFloat(detail.childage)) &&
          !isNaN(parseFloat(detail.tuition))
      ) {
        return (
          (parseFloat(eduPlanAge) - parseFloat(detail.childage)) *
          parseFloat(detail.tuition) *
          12
        );
      }
      return 0;
    });
    setTotalProtect(totalProtectArr.reduce((sum, item) => (sum += item), 0));
  };
  const handleChangeChildAge = (e) => {
    const childage = e.target.value;
    const eduPlanAge = valuesRef.current.eduPlanAge;
    const childrenDetails = [...valuesRef.current.childrenDetails];
    const totalProtectArr = childrenDetails.map((detail, index) => {
      if(
        !isNaN(parseFloat(eduPlanAge)) &&
          !isNaN(parseFloat(childage)) &&
          !isNaN(parseFloat(detail.tuition))
      ) {
        return (
          (parseFloat(eduPlanAge) - parseFloat(childage)) *
          parseFloat(detail.tuition) *
          12
        );
      }
      return 0
    });
    setTotalProtect(totalProtectArr.reduce((sum, item) => (sum += item), 0));
  };
  const handleChangeTuition = (e) => {
    const tuition = e.target.value;
    const eduPlanAge = valuesRef.current.eduPlanAge;
    const childrenDetails = [...valuesRef.current.childrenDetails];
    const totalProtectArr = childrenDetails.map((detail, index) => {
      if(
        !isNaN(parseFloat(eduPlanAge)) &&
          !isNaN(parseFloat(detail.childage)) &&
          !isNaN(parseFloat(tuition))
      ) {
        return (
          (parseFloat(eduPlanAge) - parseFloat(detail.childage)) *
          parseFloat(tuition) *
          12
        );
      }
      return 0;
    });
    setTotalProtect(totalProtectArr.reduce((sum, item) => (sum += item), 0));
  };

  const handleBlurInput = () => {
    const values = formRef.current.values
    dispatch(setFormValueProtect({
      ...values,
      total: totalProtect
    }));
    let newChidrenDetails = [];
    [...Array(values.children).keys()].map((_, index) => newChidrenDetails.push(initialValuesEdu.childrenDetails[0]));
    let newChildrenDetailsProtect = [...values.childrenDetails].map((detail, index) => {
      let newObj = {};
      Object.keys(detail).forEach((e, i) => {
        if(newChidrenDetails[index].hasOwnProperty(e)) {
          newObj[e] = parseInt(detail[e])
        }
      })
      return newObj;
    });
    newChidrenDetails = newChidrenDetails.map((detail, index) => {
      return {
        ...detail,
        ...newChildrenDetailsProtect[index]
      }
    })
    dispatch(setFormValueEdu({
      children: values.children,
      childrenDetails: newChidrenDetails
    }))
  }
  return (
    <Box>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        innerRef={formRef}
        onSubmit={(values) => {
          dispatch(setFormValueProtect({
            ...values,
            total: totalProtect
          }));
          let newChidrenDetails = [];
          [...Array(values.children).keys()].map((_, index) => newChidrenDetails.push(initialValuesEdu.childrenDetails[0]));
          let newChildrenDetailsProtect = [...values.childrenDetails].map((detail, index) => {
            let newObj = {};
            Object.keys(detail).forEach((e, i) => {
              if(newChidrenDetails[index].hasOwnProperty(e)) {
                newObj[e] = parseInt(detail[e])
              }
            })
            return newObj;
          });
          newChidrenDetails = newChidrenDetails.map((detail, index) => {
            return {
              ...detail,
              ...newChildrenDetailsProtect[index]
            }
          })
          dispatch(setFormValueEdu({
            children: values.children,
            childrenDetails: newChidrenDetails
          }))
          dispatch(setStep(STEP_TWO))
        }}
      >
        {({ values }) => {
          valuesRef.current = values;
          return (
            <Form>
              <Paper variant="elevation" className={classes.payper}>
                <Typography variant="h5" className={classes.title}>
                  Bảo vệ kế hoạch giáo dục cho con
                </Typography>
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Typography variant="body1">
                    Gia đình bạn có bao nhiêu con ?
                  </Typography>
                  <Field
                    name="children"
                    component={InputCount}
                    type="number"
                    onIncrement={handleIncrement}
                    onDecrement={handleDecrement}
                    count={child}
                  />
                </Box>
                <FieldArray name="childrenDetails">
                  {(arrayHelper) => {
                    arrayHelperRef.current = arrayHelper;
                    return (
                      <>
                        {values.childrenDetails.map((_, index) => (
                          <InputGroupProtect
                            key={index}
                            label1={`Con thứ ${index + 1}`}
                            label2="Tiền học hàng tháng"
                            name1={`childrenDetails.${index}.childage`}
                            name2={`childrenDetails.${index}.tuition`}
                            changeChildAge={handleChangeChildAge}
                            changeTuition={handleChangeTuition}
                            onBlurCustom={handleBlurInput}
                          />
                        ))}
                      </>
                    );
                  }}
                </FieldArray>
                <Typography variant="body1">
                  Bạn muốn bảo vệ kế hoạch giáo dục cho các con đến năm bao
                  nhiêu tuổi ?
                </Typography>
                <Field
                  name="eduPlanAge"
                  component={InputButtonGroup}
                  btnLb="Tuổi"
                  placeholder="Nhập"
                  customOnchange={handleChangeEduPlanAge}
                  type="number"
                  onBlurCustom={handleBlurInput}
                />
                <Box
                  sx={{
                    marginTop: "20px",
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <Typography variant="h6" className={classes.text}>
                    Số tiền thu nhập bạn cần bảo vệ là:
                  </Typography>
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      backgroundColor: "#B4AAB3",
                      padding: "10px 50px",
                      borderRadius: "10px",
                      marginLeft: "20px",
                    }}
                  >
                    <Typography
                      variant="h3"
                      sx={{ color: "#7F5EB4", marginRight: "10px" }}
                    >
                      {!isNaN(parseFloat(totalProtect)) ? totalProtect : 0}
                    </Typography>
                    <Typography variant="h6" sx={{ color: "#7F5EB4" }}>
                      Triệu đồng
                    </Typography>
                  </Box>
                </Box>
              </Paper>

              <Box
                sx={{
                  display: "flex",
                  marginTop: "30px",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Button
                  type="submit"
                  variant="contained"
                  color="secondary"
                  disabled={!(totalProtect > 0)}
                >
                  Tiếp tục
                </Button>
              </Box>
            </Form>
          );
        }}
      </Formik>
    </Box>
  );
}

export default ProtectEducationPlan;

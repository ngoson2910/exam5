import { Grid } from "@mui/material";
import { Box } from "@mui/system";
import { FastField, Field } from "formik";
import React from "react";
import PropTypes from 'prop-types'
import InputButtonGroup from "../../customFields/InputButtonGroup";
import { inputGroupProtectStyle } from "./style";

InputGroupProtect.propTypes = {
  label1: PropTypes.string,
  label2: PropTypes.string,
  name1: PropTypes.string,
  name2: PropTypes.string,
  changeChildAge: PropTypes.func,
  changeTuition: PropTypes.func,
}

InputGroupProtect.defaultProps = {
  label1: '',
  label2: '',
  name1: '',
  name2: '',
  changeChildAge: () => {},
  changeTuition: () => {},
}

function InputGroupProtect(props) {
  const classes = inputGroupProtectStyle();
  const {label1, label2, name1, name2, changeChildAge, changeTuition, onBlurCustom} = props
  return (
    <Box sx={{
      marginBottom: '20px'
    }}>
      <Grid container spacing={2}>
        <Grid item xs={5}>
          <Field
            name={name1}
            component={InputButtonGroup}
            label={label1}
            btnLb="Tuổi"
            placeholder="Nhập"
            customOnchange={changeChildAge}
            type="number"
            onBlurCustom={onBlurCustom}
          />
        </Grid>
        <Grid item xs={7}>
          <Field
            name={name2}
            component={InputButtonGroup}
            label={label2}
            btnLb="Triệu đồng"
            placeholder="Nhập"
            customOnchange={changeTuition}
            type="number"
            onBlurCustom={onBlurCustom}
          />
        </Grid>
      </Grid>
    </Box>
  );
}

export default InputGroupProtect;

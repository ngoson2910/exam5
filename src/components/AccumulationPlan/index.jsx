import { Box, Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import { FastField, Field, Form } from "formik";
import React, { useState } from "react";
import CheckboxField from "../../customFields/CheckboxFieldMUI";
import InputButtonGroup from "../../customFields/InputButtonGroup";
import { AccumulationPlanStyle } from "./style";

function AccumulationPlan(props) {
  const { labelCheckbox, name1, name2, name3, isChecked, onChangeFeeAndYear, onBlurCustom } =
    props;
  const classes = AccumulationPlanStyle();
  const [checked, setChecked] = useState(isChecked);

  const handleChecked = (e) => {
    setChecked(e.target.checked)
  }
  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
      }}
    >
      <FormGroup sx={{ width: "15%" }}>
        <Field
          name={name1}
          component={CheckboxField}
          label={labelCheckbox}
          checked={checked}
          onChecked={handleChecked}
          onBlurCustom={onBlurCustom}
        />
      </FormGroup>
      <FormGroup sx={{ width: "45%" }}>
        <Field
          name={name2}
          component={InputButtonGroup}
          label="Số tiền/năm"
          btnLb="Triệu đồng"
          placeholder="Nhập"
          type="number"
          disabled={!checked}
          customOnchange={onChangeFeeAndYear}
          onBlurCustom={onBlurCustom}
        />
      </FormGroup>
      <FormGroup sx={{ width: "40%" }}>
        <Field
          name={name3}
          component={InputButtonGroup}
          label="Số năm học"
          btnLb="Năm"
          placeholder="Nhập"
          type="number"
          disabled={!checked}
          customOnchange={onChangeFeeAndYear}
          onBlurCustom={onBlurCustom}
        />
      </FormGroup>
    </Box>
  );
}

export default AccumulationPlan;

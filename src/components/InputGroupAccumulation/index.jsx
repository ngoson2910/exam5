import { Box, FormGroup, Typography } from "@mui/material";
import { FastField, Field } from "formik";
import React from "react";
import InputButtonGroup from "../../customFields/InputButtonGroup";
import AccumulationPlan from "../AccumulationPlan";
import { inputGroupAccumulationStyle } from "./style";

function InputGroupAccumulation(props) {
  const classes = inputGroupAccumulationStyle();
  const { label, index, childrenDetails, onChangeFeeAndYear, onCustomBlur } = props;
  return (
    <Box sx={{ marginBottom: "20px" }}>
      <FormGroup sx={{ width: "40%" }}>
        <Field
          name={`childrenDetails.${index}.childage`}
          component={InputButtonGroup}
          label={label}
          btnLb="Tuổi"
          placeholder="Nhập"
          type="number"
          onBlurCustom={onCustomBlur}
        />
      </FormGroup>

      <Typography
        variant="body1"
        sx={{ marginTop: "20px", marginBottom: "20px" }}
      >
        Bạn muốn lập kế hoạch tích lũy cho giáo dục của con ở các bậc học nào?
      </Typography>
      <AccumulationPlan
        labelCheckbox="Phổ thông"
        name1={`childrenDetails.${index}.isSchool`}
        name2={`childrenDetails.${index}.schoolFee`}
        name3={`childrenDetails.${index}.schoolYear`}
        isChecked={childrenDetails.isSchool}
        onChangeFeeAndYear={onChangeFeeAndYear}
        onBlurCustom={onCustomBlur}
      />
      <AccumulationPlan
        labelCheckbox="Đại học"
        name1={`childrenDetails.${index}.isUniversity`}
        name2={`childrenDetails.${index}.universityFee`}
        name3={`childrenDetails.${index}.universityYear`}
        isChecked={childrenDetails.isUniversity}
        onChangeFeeAndYear={onChangeFeeAndYear}
        onBlurCustom={onCustomBlur}
      />
      <AccumulationPlan
        labelCheckbox="Sau đại học"
        name1={`childrenDetails.${index}.isAfterUniversity`}
        name2={`childrenDetails.${index}.afterUniversityFee`}
        name3={`childrenDetails.${index}.afterUniversityYear`}
        isChecked={childrenDetails.isAfterUniversity}
        onChangeFeeAndYear={onChangeFeeAndYear}
        onBlurCustom={onCustomBlur}
      />
    </Box>
  );
}

export default InputGroupAccumulation;

import { Button, OutlinedInput, TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Box } from "@mui/system";
import PropTypes from "prop-types";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

const useStyle = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  minus: {
    position: "absolute !important",
    padding: "0px !important",
    minWidth: "unset !important",
    top: "50%",
    left: "15px",
    transform: "translateY(-50%)",
    zIndex: 10,
  },
  plus: {
    position: "absolute !important",
    padding: "0px !important",
    minWidth: "unset !important",
    top: "50%",
    right: "15px",
    transform: "translateY(-50%)",
    zIndex: 10,
  },
}));

InputCount.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  onIncrement: PropTypes.func,
  onDecrement: PropTypes.func,
  count: PropTypes.number
};

InputCount.defaultProps = {
  type: "text",
  placeholder: "",
  disabled: false,
};

function InputCount(props) {
  const classes = useStyle();
  const { field, form, type, placeholder, disabled, onIncrement, onDecrement, count } =
    props;
  const { name } = field;

  // const handleCountValue = (e) => {
  //   console.log('e.target.value', e.target.value);
  //   form.setFieldValue(name, e.target.value)
  // }
  return (
    <Box
      sx={{
        width: "150px",
        position: "relative",
        marginLeft: "20px",
      }}
    >
      <Button
        variant="outlined"
        className={classes.minus}
        onClick={onDecrement}
      >
        <RemoveIcon />
      </Button>
      <TextField
        type={type}
        id={name}
        className={classes.root}
        {...field}
        disabled={disabled}
        placeholder={placeholder}
        variant="outlined"
        inputProps={{
          style: { textAlign: "center", padding: "10px" },
          readOnly: true,
        }}
        // value={count}
        // onChange={(e) => handleCountValue(e)}
      />

      <Button variant="outlined" className={classes.plus} onClick={onIncrement}>
        <AddIcon />
      </Button>
    </Box>
  );
}

export default InputCount;

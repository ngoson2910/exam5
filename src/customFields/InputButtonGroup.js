import { Button, ButtonGroup, FormHelperText, TextField, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Box } from "@mui/system";
import PropTypes from "prop-types";

const useStyle = makeStyles((theme) => ({
  btnGroup: {
    width: '68%'
  },
  root: {
    width: '60%'
  },
  label: {
    marginRight: '15px !important'
  }
}));

InputButtonGroup.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  btnLb: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  customOnchange: PropTypes.func,
  onBlurCustom: PropTypes.func,
};

InputButtonGroup.defaultProps = {
  type: "text",
  label: "",
  btnLb: "",
  placeholder: "",
  disabled: false,
  customOnchange: () => {},
  onBlurCustom: () => {},
};

function InputButtonGroup(props) {
  const classes = useStyle();
  const { field, form, type, label, btnLb, placeholder, disabled, customOnchange, onBlurCustom } = props;
  const { name } = field;
  const { errors, touched } = form;
  const nameArr = name.split('.');
  let showError = false;
  if(nameArr.length > 1) {
    showError = errors[nameArr[0]] && touched[nameArr[0]] ? true : false;
  } else {
    showError = errors[name] && touched[name] ? true : false;
  }
  

  const handleChangeTextField = (formikChange, e) => {
    const value = e.target.value
    form.setFieldValue(name, !isNaN(value) ? value: '')
    customOnchange(e)
  }
  function handleBlur(formikBlur, event) {
    formikBlur(event);
    onBlurCustom()
  }
  return (
    <>
    <Box
      sx={{
        width: "100%",
        marginTop: "20px",
        display: "flex",
        alignItems: 'center',
        justifyContent: 'space-between'
      }}
    >
      {label && <Typography variant="body1" className={classes.label}>{label}</Typography>}
      <ButtonGroup variant="outlined" className={classes.btnGroup}>
        <TextField
          type={type}
          id={name}
          {...field}
          disabled={disabled}
          placeholder={placeholder}
          autoComplete="off"
          variant="outlined"
          InputProps={{ style: { borderRadius: '8px 0 0 8px'}}}
          inputProps={{style: { padding: '10px' }}}
          className={classes.root}
          onChange={(e) => handleChangeTextField(field.onChange,e)}
          onBlur={e => handleBlur(field.onBlur, e)}
        />
        {btnLb && <Button
          sx={{borderRadius: '0 8px 8px 0', textTransform: 'capitalize'}}
        >{btnLb}</Button>}
      </ButtonGroup>
    </Box>
    {showError && <FormHelperText style={{color: 'red'}}>{errors[name] || (typeof errors[nameArr[0]][nameArr[1]] !== 'undefined' && errors[nameArr[0]][nameArr[1]][nameArr[2]])}</FormHelperText>}
    </>
  );
}

export default InputButtonGroup;

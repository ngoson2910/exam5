import { Checkbox, FormControlLabel, FormGroup, FormHelperText } from "@mui/material";
import PropTypes from "prop-types";

CheckboxField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  onBlurCustom: PropTypes.func
};

CheckboxField.defaultProps = {
  type: "checkbox",
  label: "",
  checked: false,
  disabled: false,
  onBlurCustom: () => {}
};

function CheckboxField(props) {
  const { field, form, type, label, disabled, checked, onChecked,onBlurCustom } = props;
  const { name } = field;
  const { errors, touched } = form;
  const nameArr = name.split('.');
  let showError = false;
  if(nameArr.length > 1) {
    showError = errors[nameArr[0]] && touched[nameArr[0]] ? 'true' : undefined;
  } else {
    showError = errors[name] && touched[name] ? 'true' : undefined;
  }
  const handleChecked = (e) => {
    form.setFieldValue(name, e.target.checked)
    onChecked(e)
  }

  const handleBlur = (formikBlur, e) => {{
    formikBlur(e)
    onBlurCustom()
  }}
  return (
    <FormGroup>
      <FormControlLabel
      sx={{ marginTop: "20px" }}
      control={
        <Checkbox
          name={name}
          checked={checked}
          {...field}
          disabled={disabled}
          error={showError}
          id={name}
          onChange={(e) => handleChecked(e)}
          onBlur={e => handleBlur(field.onBlur, e)}
        />
      }
      label={label}
    />
    {showError && <FormHelperText style={{color: 'red'}}>{errors[name] || (typeof errors[nameArr[0]][nameArr[1]] !== 'undefined' && errors[nameArr[0]][nameArr[1]][nameArr[2]])}</FormHelperText>}
    </FormGroup>
  );
}

export default CheckboxField;

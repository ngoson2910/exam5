import { TextField } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Box } from '@mui/system';
import PropTypes from 'prop-types'

const useStyle = makeStyles(theme => ({
  input: {
    width: '100%'
  }
}))


InputField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool
}

InputField.defaultProps = {
  type: 'text',
  label: '',
  placeholder: '',
  disabled: false
}

function InputField(props) {
  const classes = useStyle();
  const {
    field, form,
    type, label, placeholder, disabled
  } = props
  const { name }  = field;
  const { errors, touched } = form
  const showError = errors[name] && touched[name] ? true: false
  return (
    <Box 
      sx={{
        width: '100%',
        marginTop: '20px'
      }}
    >
      <TextField
        label={label}
        type={type}
        id={name}
        className={classes.input}
        error={showError}
        {...field}
        disabled={disabled}
        placeholder={placeholder}
        autoComplete='off'
        helperText={errors[name]}
      />
    </Box>
  )
}

export default InputField;
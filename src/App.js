import { Box, LinearProgress } from "@mui/material";
import React, { Suspense } from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Header from "./components/Header";
import NotFound from "./pages/NotFound";

const Home = React.lazy(() => import("./pages/Home"));
const Insurrance = React.lazy(() => import("./pages/Insurrance"));
const Login = React.lazy(() => import("./pages/Login"));

function App() {
  const {user} = useSelector(state => state.auth)
  return (
    <Suspense
      fallback={
        <Box sx={{ width: "100%" }}>
          <LinearProgress />
        </Box>
      }
    >
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/insurrance" element={<Insurrance />} />
          {user
            ? (<Route path="/login" element={<Navigate to="/" />} />)
            : (<Route path="/login" element={<Login />} />)
            }
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;

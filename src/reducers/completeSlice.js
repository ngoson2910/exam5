import { createSlice } from "@reduxjs/toolkit";


const completeSlice = createSlice({
  name: "complete",
  initialState: {
    completed: false
  },
  reducers: {
    complete(state, action) {
      state.completed = action.payload
    }
  }
})

const { actions, reducer} = completeSlice
export const { complete } = actions
export default reducer
import { createSlice } from "@reduxjs/toolkit";

export const initialValuesEdu = {
  children: 1,
  total: 0,
  childrenDetails: [
    {
      childage: '',
      isSchool: true,
      schoolFee: '',
      schoolYear: '',
      isUniversity: false,
      universityFee: '',
      universityYear: '',
      isAfterUniversity: false,
      afterUniversityFee: '',
      afterUniversityYear: '',
    },
  ],
}

const eduPlanSlice = createSlice({
  name: "eduPlan",
  initialState: initialValuesEdu,
  reducers: {
    setFormValueEdu(state, action) {
      const newState = {
        ...state,
        ...action.payload,
      };
      return newState;
    },
  },
});

const { actions, reducer } = eduPlanSlice;
export const { setFormValueEdu } = actions;
export default reducer;

import { createSlice } from "@reduxjs/toolkit";

export const initialValuesProtect = {
  children: 1,
  total: 0,
  eduPlanAge: '',
  childrenDetails: [
    {
      childage: '',
      tuition: '',
    },
  ],
}

const productEduSlice = createSlice({
  name: "productEdu",
  initialState: initialValuesProtect,
  reducers: {
    setFormValueProtect(state, action) {
      const newState = {
        ...state,
        ...action.payload,
      };
      return newState;
    },
  },
});

const { actions, reducer } = productEduSlice;
export const { setFormValueProtect } = actions;
export default reducer;

import { combineReducers } from 'redux'
import stepSlice from './stepSlice'
import completeSlice from './completeSlice'
import authSlice from './authSlice'
import productEduSlice from './productEduSlice'
import eduPlanSlice from './eduPlanSlice'

const rootReducer = combineReducers({
    step: stepSlice,
    complete: completeSlice,
    auth: authSlice,
    productEdu: productEduSlice,
    eduPlan: eduPlanSlice
})

export default rootReducer
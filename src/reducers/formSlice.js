import { createSlice } from "@reduxjs/toolkit";

const formSlice = createSlice({
  name: "form",
  initialState: {
    firstName: "",
    lastName: "",
    email: "",
    confirmEmail: "",
    password: "",
    confirmPassword: "",
    accept: false,
  },
  reducers: {
    setFormValue(state, action) {
      const newState = {
        ...state,
        ...action.payload
      }
      return newState
    }
  }
});

const {actions, reducer} = formSlice
export const {setFormValue} = actions
export default reducer

import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import authApi from "../api/authApi";

export const login = createAsyncThunk('auth/login', async(params, thunkApi) => {
  const {username, password} = params
  const user = await authApi.login(username, password)
  return user
})

const authSlice = createSlice({
  name: "auth",
  initialState: {
    user: JSON.parse(localStorage.getItem('user')),
    loading: false,
    error: ''
  },
  reducers: {
    logout: (state, action) => {
      state.user = null
    }
  },
  extraReducers: {
    [login.pending]: (state) => {
      state.loading = true
    },
    [login.rejected]: (state, action) => {
      state.loading = false
      state.error = action.error.message
    },
    [login.fulfilled]: (state, action) => {
      state.loading = false
      state.error = ''
      state.user = action.payload
      localStorage.setItem('user', JSON.stringify(action.payload))
    }
  }
})

const {actions, reducer} = authSlice
export const { logout } = actions
export default reducer
import React, { useState } from "react";
import { Alert, Box, Snackbar } from "@mui/material";
import Banner from "../components/Banner";
import { useSelector } from "react-redux";

function Home() {
  const [open, setOpen] = useState(true);
  const { user } = useSelector((state) => state.auth);
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <Box>
      {user && (
        <Snackbar
          open={open && user ? true : false}
          autoHideDuration={2000}
          onClose={handleClose}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
        >
          <Alert severity="success" sx={{ width: "100%" }}>
            Login susscessfully !!!
          </Alert>
        </Snackbar>
      )}
      <Banner title="Home" />
    </Box>
  );
}

export default Home;

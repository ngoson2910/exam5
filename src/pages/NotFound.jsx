import { Box } from '@mui/material'
import React from 'react'
import Banner from '../components/Banner'


function NotFound() {
  return (
    <Box>
      <Banner title="Oppss.... Not Found!!!!!"  />
    </Box>
  )
}

export default NotFound

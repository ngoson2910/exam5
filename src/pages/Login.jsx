import React from "react";
import { Alert, AlertTitle, Box, Button, Container, Grid, Snackbar } from "@mui/material";
import { FastField, Form, Formik } from "formik";
import * as Yup from "yup";
import Image from "../constants/image";
import Banner from "../components/Banner";
import InputField from "../customFields/InputField";
import { makeStyles } from "@mui/styles";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../reducers/authSlice";
import { LoadingButton } from "@mui/lab";

const useStyle = makeStyles((theme) => ({
  button: {
    width: "100%",
    marginTop: "20px !important",
  },
  alert: {
    marginTop: "30px",
  },
}));

function Login() {
  const { user, loading, error } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const classes = useStyle();
  const initialValues = {
    username: "",
    password: "",
    passwordConfirmation: "",
  };
  const validationSchema = Yup.object().shape({
    password: Yup.string()
      .required()
      .oneOf([Yup.ref("passwordConfirmation"), null], ""),
    passwordConfirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Password must match"
    ),
  });
  return (
    <Box>
      <Banner title="Login" backgroundUrl={Image.PINK_BG} />
      <Container>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={4}>
            {error && (
              <Alert severity="error" className={classes.alert}>
                <AlertTitle>Error</AlertTitle>
                {error}
              </Alert>
            )}
          
            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                dispatch(login(values));
              }}
            >
              {(formikProps) => {
                return (
                  <Form>
                    <FastField
                      name="username"
                      component={InputField}
                      label="Username"
                      placeholder="Enter your username"
                    />
                    <FastField
                      name="password"
                      component={InputField}
                      label="Password"
                      type="password"
                      placeholder="Enter your password"
                    />
                    <FastField
                      name="passwordConfirmation"
                      component={InputField}
                      label="Confirm Password"
                      type="password"
                      placeholder="Confirm your password"
                    />
                    <LoadingButton
                      type="submit"
                      variant="contained"
                      color="primary"
                      className={classes.button}
                      loading={loading}
                    >
                      Login
                    </LoadingButton>
                  </Form>
                );
              }}
            </Formik>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}

export default Login;

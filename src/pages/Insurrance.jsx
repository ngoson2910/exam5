import React, { useState } from "react";
import { Box, Container, Grid, Paper } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Banner from "../components/Banner";
import Image from "../constants/image";
import SideBar from "../components/Insurrance/SideBar";
import ProtectEducationPlan from "../components/Insurrance/ProtectEducationPlan";
import AccumulationEducationPlan from "../components/Insurrance/AccumulationEducationPlan";
import { useSelector } from "react-redux";
import { STEP_ONE, STEP_TWO } from "../constants/step";

const useStyle = makeStyles((theme) => ({
  grid: {
    marginTop: "30px !important",
    marginBottom: '100px'
  },
  paper: {
    minHeight: "500px",
    padding: "20px",
  },
}));

function Insurrance() {
  const classes = useStyle();
  const { currentStep } = useSelector((state) => state.step);
  return (
    <Box>
      <Banner title="Insurrance" backgroundUrl={Image.PINK_BG} />
      <Container maxWidth="xl">
        <Grid container spacing={4} className={classes.grid}>
          <Grid item xs={3}>
            <Paper variant="elevation" className={classes.paper}>
              <SideBar />
            </Paper>
          </Grid>
          <Grid item xs={9}>
            {currentStep === STEP_ONE && <ProtectEducationPlan />}
            {currentStep === STEP_TWO && <AccumulationEducationPlan />}
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}

export default Insurrance;
